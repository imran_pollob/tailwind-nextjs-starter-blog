---
date: ! '2021/06/26 '
title: Codewars - convert a string to an array solution
tags:
  - codewars
  - string
  - array
summary: ''
link: https://www.codewars.com/kata/57e76bc428d6fbc2d500036d
draft: false
---

```js
function stringToArray(string) {
  return string.split(' ')
}
```
