---
date: ! '2021/07/01 '
title: Codesignal - add
tags:
  - codesignal
summary: ''
link: https://app.codesignal.com/arcade/intro/level-1/jwr339Kq6e3LQTsfa
draft: false
---

```python
def add(param1, param2):
    return param1 + param2
```
