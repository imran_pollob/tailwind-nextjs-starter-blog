# Code-UI

This is the web version of my solved coding solutions. Please check out this site [https://code.imranpollob.com/](https://code.imranpollob.com/)

Link of the original repo: [https://github.com/pollmix/code/](https://github.com/pollmix/code/)

Show some support by giving me a star ⭐️

> Web starter template used: [tailwind-nextjs-starter-blog](https://github.com/timlrx/tailwind-nextjs-starter-blog)
